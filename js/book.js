var total_pages;
var url = "./test.pdf";
var flipbookElm = document.getElementById("flipbook");
var p1viewport;
var scale = 2.5; //render rez / scale

var progressTextPages = document.getElementById("lProgressPages");
var progressTextPerc = document.getElementById("lProgressPrec");

var eventBookReady = new Event('bookReady');

var pdf;
var svgmode = true;
var noshadow = svgmode; //for some reason, svg rendering got shadow

var booktexts = [];
var page1test;

// get the ?pdf=foo.pdf parameter
let params = new URLSearchParams(document.location.search.substring(1));
var pdfURL = params.get("pdf");

if (pdfURL !== null && pdfURL != "") {
  url = pdfURL;
  console.log("found new url")
  console.log(pdfURL)
}

document.getElementById('btnDownloadA').setAttribute("href", url);

(async () => {
  const loadingTask = pdfjsLib.getDocument(url);
  pdf = await loadingTask.promise;
  total_pages = pdf.numPages;
  console.log(total_pages);
  let bookname = url.split("/").pop().split(".").slice(0, -1).join(".");
  document.getElementById("bookNameElm").innerText = bookname;
  document.title = bookname + " - ספר הפלא"
  renderPdfBook(pdf);
})();

async function renderPdfBook(pdf) {
  console.time("book render");
  document.getElementById("book").style.display = "none";
  document.getElementById("controls").style.display = "none";
  document.getElementById("rangeControl").style.display = "none";
  document.getElementById("loadingDiv").style.display = "block";
  progressTextPages.innerHTML = "0 / " + total_pages;
  progressTextPerc.innerHTML = "0%";

  for (let pagenum = 1; pagenum <= total_pages; pagenum++) {
    // Load information from the first page.
    const page = await pdf.getPage(pagenum);

    const viewport = page.getViewport(scale);
    if (svgmode) { // render with svg
      booktexts[pagenum] = page.getTextContent(); //returns a promise
      booktexts[pagenum].then((values) => booktexts[pagenum] = values); //make the promise a val obj
      page1test = page;
      await page.getOperatorList()
        .then(function (opList) {
          var svgGfx = new pdfjsLib.SVGGraphics(page.commonObjs, page.objs);
          return svgGfx.getSVG(opList, viewport);
        })
        .then(function (svg) {
          var newDiv = document.createElement("div");
          newDiv.appendChild(svg)
          flipbookElm.appendChild(newDiv);
        });

    } else { //svg mode off ( canvas render )

      var newCanvas = document.createElement("canvas");

      const context = newCanvas.getContext("2d");
      newCanvas.height = viewport.height;
      newCanvas.width = viewport.width;

      flipbookElm.appendChild(newCanvas);

      // Render the page into the <canvas> element.
      const renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      await page.render(renderContext);
    }

    console.log("Page rendered!");
    progressTextPages.innerHTML = pagenum + " / " + total_pages;
    progressTextPerc.innerHTML = Math.round((pagenum / total_pages) * 100) + "%";
  }

  const page1 = await pdf.getPage(1);
  p1viewport = page1.getViewport(scale);
  var pageHeight = p1viewport.width,
    pageWidth = p1viewport.height;
  var cHeight = document.documentElement.clientHeight,
    cWidth = document.documentElement.clientWidth;
  let ratio = cHeight / cWidth;
  var factor;
  if (cWidth <= cHeight * ratio) {
    factor = (cWidth / 1.3) / pageWidth;
  } else {
    factor = (cHeight / 1.4) / pageHeight;
  }

  //let sViewport = pdf.getPage(1).getViewport(scale);
  $("#flipbook").turn({
    width: p1viewport.height * factor,
    height: p1viewport.width * factor,
    autoCenter: true,
    acceleration: true,
    direction: "rtl"
  });

  document.getElementById("zoom-viewport").style.marginLeft = (cWidth - (pageWidth * factor)) / 2 + "px";
  //document.getElementById("flipbook").style.transform = "translate3d("+ (document.documentElement.clientWidth - p1viewport.height)/2 +"px, 0px, 0px)"

  let shadowElm = document.createElement("div");
  shadowElm.classList.add("bookShadow");
  flipbookElm.appendChild(shadowElm);
  shadowElm.style.display = "none";

  document.body.dispatchEvent(eventBookReady);
  console.timeEnd("book render");

  //after the book is rendered
  //flip to the page num in the hash if there is
  if (window.location.hash != "") {
    let instaPage = window.location.hash.substr(1);
    $("#flipbook").turn("page", instaPage);
  }

  //add the page num to the hash
  $("#flipbook").bind("turned", function (event, page, view) {
    updatePageCounter();
    document.getElementById("pageRange").value = page;
    window.location.hash = "#" + page;
  });
  //hide the fake shadow between the pages at page 1 and the end of the book
  $("#flipbook").bind("turning", function (event, page, view) {
    if (page == 1 || page == total_pages) {
      shadowElm.style.display = "none";
    } else {
      if (!noshadow) {
        shadowElm.style.display = "inline";
      }
    }
  });

  document.getElementsByClassName("bookShadow")[0].style.zIndex = total_pages + 5;

  updatePageCounter();
  document.getElementById("pageRange").setAttribute("max", total_pages);
  document.getElementById("pageRange").value = 1;
  document.getElementById("pageInput").setAttribute("max", total_pages);
  document.getElementById("pageInput").value = "";

  document.getElementById("book").style.display = "";
  document.getElementById("controls").style.display = "";
  document.getElementById("rangeControl").style.display = "";
  document.getElementById("loadingDiv").style.display = "none";

  inputShareUrlChanger();

}

class SVGGraphicsOverride extends pdfjsLib.SVGGraphics {
  constructor(commonObjs, objs, idMap) {
    super(commonObjs, objs);
    this._currentSvgText = "";
    this._idMap = idMap;
  }
  showText(glyphs) {
    for (const glyph of glyphs) {
      if (glyph && (glyph.unicode !== undefined))
        this._currentSvgText += glyph.unicode;
    }
    super.showText(glyphs);
  }
  endText() {
    super.endText();
    const svgText = this._currentSvgText;
    this._currentSvgText = "";

    const point = this._idMap.get(svgText);
    if (point) {
      if (point.ID === undefined) {
        const id = `ap-${nextId.getNext()}`;
        this.current.txtElement.setAttribute("id", id);
        point.ID = id;
      }
    }
  }
}