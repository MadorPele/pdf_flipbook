var isZoomed = false;
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var reRender = false;
var bookZoomLvl = 1.6;

document.onkeydown = function (e) {
    e = e || window.event;
    switch (e.which || e.keyCode) {
        case 37: // left
            $("#flipbook").turn("next");
            break;

        case 39: // right
            $("#flipbook").turn("previous");
            break;

        default:
            return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
};

document.body.onwheel = wheelEvent;
function zoomBook() {
    //ransform: translate3d(379.04px, 186px, 0px) scale(1.6);
    bookZoomLvl = 1.6;
    let button = document.getElementById("zoombt");
    if (!isZoomed) {
        document.getElementById("zoom-viewport").style.overflow = "";
        //document.getElementById("flipbook").style.transform = "translate3d(379.04px, 186px, 0px) scale(1.6)";
        document.getElementById("flipbook").style.transform = "scale("+bookZoomLvl+")";
        button.src = "images/zoom-out.png";
        isZoomed = true;
        jQuery("#flipbook").draggable({
            cursor: "move",
            containment: "book",
            scroll: false
        });
        document.getElementById("flipbook").style.transitionDuration = "0s";
    } else {
        //document.getElementById("flipbook").style.transform = "translate3d(379.04px, 0px, 0px)";
        document.getElementById("flipbook").style.transitionDuration = "1s";
        document.getElementById("flipbook").style.transform = "";
        button.src = "images/zoom-in.png";
        isZoomed = false;
        jQuery("#flipbook").draggable("destroy");
        document.getElementById("flipbook").style.top = "";
        document.getElementById("flipbook").style.left = "";
        //document.getElementById("zoom-viewport").style.overflow = "hidden";

    }
}

function wheelEvent(event) {
    event.preventDefault();
    if(isZoomed){
        bookZoomLvl += event.deltaY * -0.01;
        // Restrict scale
        bookZoomLvl = Math.min(Math.max(1.1, bookZoomLvl), 3);
        // Apply scale transform
        document.getElementById("flipbook").style.transform = "scale("+bookZoomLvl+")";
        console.log(bookZoomLvl);
    }  
}

function updatePageCounter() {
    var currentPage = $("#flipbook").turn("page");
    if (currentPage == 1 || currentPage == total_pages) {
        document.getElementById("pageCounter").innerHTML = total_pages + " \\ " + currentPage;
    } else {
        if (currentPage % 2 == 0) {
            document.getElementById("pageCounter").innerHTML = total_pages + " \\ " + (currentPage + 1) + "-" + (currentPage);
        } else {
            document.getElementById("pageCounter").innerHTML = total_pages + " \\ " + currentPage + "-" + (currentPage - 1);
        }
    }
}

document.addEventListener("fullscreenchange", function () {
    fullscreenBTN(true);
});

function fullscreenBTN(isEvent = false) {
    let btn = document.getElementById("fullscrnBtn");

    if ((window.fullScreen) ||
        (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
        if (!isEvent) {
            closeFullscreen();
        }
        btn.classList.remove("btnActive");
    } else {
        if (!isEvent) {
            openFullscreen();
        }
        btn.classList.add("btnActive");
    }
    if (isIframe() || isChrome || isSafari) {
        $("#flipbook").turn("destroy");
        document.getElementById("flipbook").innerHTML = "";
        reRender = true;
        renderPdfBook(pdf);
        /*setTimeout(function(){ //WTF WHY?????????? idk why it works
            $("#flipbook").turn("destroy");
            document.getElementById("flipbook").innerHTML = "";
            renderPdfBook(pdf);
        },1500);*/
    }

}
document.body.addEventListener("bookReady", function (e) {
    if (reRender) {
        console.log("RERENDER EVENT")
        reRender = false;
        setTimeout(function () { //WTF WHY?????????? idk why it works
            $("#flipbook").turn("destroy");
            document.getElementById("flipbook").innerHTML = "";
            renderPdfBook(pdf);
        }, 1500);
    }
}, false);

function openFullscreen() {
    if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    }
}

function isIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function rangeToPage() {
    let p = document.getElementById("pageRange").value;
    $("#flipbook").turn("page", p);
}

function showPageInput() {
    var inpElm = document.getElementById("pageInput");
    inpElm.value = "";
    inpElm.style.display = "inline";
    inpElm.focus();
    document.getElementById("pageCounter").style.display = "none";
}

function submitPageInput() {
    document.getElementById("pageInput").style.display = "none";
    document.getElementById("pageCounter").style.display = "inline";
    $("#flipbook").turn("page", document.getElementById("pageInput").value);
    document.body.focus();
}
/*$('input, select, textarea').on('blur', function(event) {
    $('meta[name=viewport]').attr('content', 'width=device-width,initial-scale=1,maximum-scale=' + (event.type == 'blur' ? 10 : 1));
});*/
document.getElementById("pageInput").addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        document.getElementById("pageInput").blur();
    }
});

$(function () {
    $(document).tooltip();
});

function pdfPrint() {
    if (isChrome || isSafari) {
        var wnd = window.open(url);
        wnd.document.onload = wnd.print();
    } else {
        window.open(url, '_blank', 'location=yes,height=570,width=820,scrollbars=yes,status=yes')
    }
}

function showShareBtn() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if (!(event.target.matches('#shareBtn') || event.target.matches('.dropdown-content') || event.target.matches('.dropdown-item'))) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

//updating the share button url
function inputShareUrlChanger(){
    var linkShareInput = document.getElementById("linkShareInput");
    if(!document.getElementById("pageNumShareCheck").checked){
        linkShareInput.value = window.location.origin +"/"+ window.location.search;
    }else{
        linkShareInput.value = window.location.origin +"/"+ window.location.search +""+window.location.hash;
    }
}

function copyLink(){
    var linkShareInput = document.getElementById("linkShareInput");
    linkShareInput.select();
    linkShareInput.setSelectionRange(0, 99999); /*For mobile devices*/
    /* Copy the text inside the text field */
    document.execCommand("copy");
}